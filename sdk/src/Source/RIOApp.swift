//
//  RIOApp.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import CommonLog
import APICodable

/// Main manager object of SDK
public final class RIOApp {

    public enum AppError: Error {
        /// You did not get app session before using the current function
        case noAppSession
        /// You did not login user before useing the current function
        case noUserSession
        /// Socket is offline (or still connecting) so job is aborted
        case socketDisconnected
        /// Call ID not found
        case callNotFound
    }

    public static let shared = RIOApp()

    var appSession: RIOSessionData?
    var userSession: RIOSessionData?
    var userInfo: RIOUser?
    var loggedInUserId: Int?

    /// App session is available?
    public var isAppSessionAvailable: Bool {
        return appSession != nil
    }

    /// User logged in?
    public var isLoggedIn: Bool {
        return userSession != nil && appSession != nil
    }

    public var socketState: RIOSocketState {
        return RIOSocket.shared.state
    }

    /// Import/Export App Session Data
    public var appSessionData: Data? {
        get {
            if let object = appSession {
                let jsonEncoder = JSONEncoder()
                return try? jsonEncoder.encode(object)
            }
            return nil
        }
        set {
            if let data = newValue {
                let jsonDecoder = JSONDecoder()
                appSession = try? jsonDecoder.decode(RIOSessionData.self, from: data)
            } else {
                appSession = nil
            }
            if appSession == nil {
                userSession = nil
            }
        }
    }

    /**
     Setup SDK.
     Remember to set values for `RIOAppConfiguration` before call this function.
     This function may throw error on invalid configuration.
     - Parameters:
        - allowLog: print log or not
        - alwaysTrustHost: `true` to ignore SSL (HTTPS) error
     */
    public func setup(allowLog: Bool, alwaysTrustHost: Bool) throws {
        try RIOAppConfiguration.shared.validate()
        if allowLog {
            CMLogger.shared.logType = .sync
        } else {
            CMLogger.shared.logType = .none
        }
        guard alwaysTrustHost, let url = URL(string: RIOAppConfiguration.shared.apiRoot) else { return }
        NWApiManager.shared.trustedHosts = [.init(sampleUrl: url)]
    }

}
