//
//  RIOApp+APIs.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

/// Main manager object of SDK
extension RIOApp {

    /**
     Get session. This function may throw error on invalid configuration

     - Parameters:
     - renew: reset session
     - onComlete: action on completion
     */
    public func getSession(renew: Bool = false, onComplete: @escaping (Error?) -> Void) throws {
        try RIOAppConfiguration.shared.validate()
        guard renew || appSession == nil else { return }
        appSession = nil
        try RIOSession.getSession({ data, error in
            let mSelf = RIOApp.shared
            mSelf.appSession = data
            onComplete(error)
        })
    }

    /**
     Regist user

     - Parameters:
     - name: user name to regist
     - onCompletion: action(user id, error) on finish
     */
    public func registUser(name: String, onComplete: @escaping (Int?, Error?) -> Void) throws {
        try RIOAppConfiguration.shared.validate()
        guard let token = appSession?.token else {
            throw AppError.noAppSession
        }
        try RIOUserRegist.registUser(name: name, token: token, onComplete: { data, error in
            let mSelf = RIOApp.shared
            mSelf.userInfo = data
            onComplete(data?.identifier, error)
        })
    }

    /**
     Login user

     - Parameters:
     - userId: user id to login
     - onComplete: action on finish
     */
    public func loginUser(userId: Int, onComplete: @escaping (Error?) -> Void) throws {
        try RIOAppConfiguration.shared.validate()
        guard let token = appSession?.token else {
            throw AppError.noAppSession
        }
        try RIOUserLogin.loginUser(userId: userId, token: token, onComplete: { data, error in
            let mSelf = RIOApp.shared
            mSelf.userSession = data
            if data != nil {
                mSelf.loggedInUserId = userId
            }
            onComplete(error)
        })
    }

    /**
     Get Users list

     - Parameters:
     - onComplete: action on finish
     */
    public func getUsersList(_ onComplete: @escaping ([RIOUser]?, Error?) -> Void) throws {
        try RIOAppConfiguration.shared.validate()
        guard let token = appSession?.token else {
            throw AppError.noAppSession
        }
        try RIOUserList.getUserList(token: token) { users, error in
            var result = users
            let userId = RIOApp.shared.loggedInUserId
            result?.removeAll{ $0.identifier == userId }
            onComplete(result, error)
        }
    }

}
