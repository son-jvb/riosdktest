//
//  RIOUserLogin.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import APICodable

final class RIOUserLogin {

    private struct RequestParam: Encodable {
        var token = ""
        var userId = 0

        private enum CodingKeys: String, CodingKey {
            case token
            case userId = "user_id"
        }
    }

    static func loginUser(userId: Int, token: String, onComplete: @escaping (RIOSessionData?, Error?) -> Void) throws {
        guard let url = URL(string: RIOAppConfiguration.shared.apiRoot + RIOAppConfiguration.shared.apiLogin)
        else { return }
        var param = RequestParam()
        param.token = token
        param.userId = userId
        let request = NWApiRequest(post: url, parameter: param, onSuccess: { sender, response in
            if let responser = response as? NWApiJsonResponseHandler<RIOResponse<RIOSessionData>>,
               let data = responser.responseObject {
                if data.success ?? false {
                    onComplete(data.data, nil)
                } else {
                    onComplete(nil, RIOApiError.apiError(data.code, data.message))
                }
            } else {
                onComplete(nil, RIOApiError.emptyResponseData)
            }
        }, onFailure: { sender, response, error in
            onComplete(nil, error)
        }, jsonResponseModel: RIOResponse<RIOSessionData>.self)
        try request.start()
    }

}
