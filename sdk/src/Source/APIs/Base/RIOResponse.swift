//
//  RIOResponse.swift
//
//  Created on 2021/12/09.
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

public enum RIOApiError: Error {
    /// Receive no data
    case emptyResponseData
    /// Server by server
    case apiError(Int?, String?)
}

struct RIOResponse<DataType: Decodable>: Decodable {

    var code: Int?
    var success: Bool?
    var message: String?
    var data: DataType?

}
