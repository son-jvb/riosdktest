//
//  RIOUserList.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import APICodable

final class RIOUserList {

    private struct RequestParam: Encodable {
        var token = ""

        private enum CodingKeys: String, CodingKey {
            case token
        }
    }

    static func getUserList(token: String, onComplete: @escaping ([RIOUser]?, Error?) -> Void) throws {
        guard let url = URL(string: RIOAppConfiguration.shared.apiRoot + RIOAppConfiguration.shared.apiUserList)
        else { return }
        let request = NWApiRequest(get: url, parameter: ["token": token], onSuccess: { sender, response in
            if let responser = response as? NWApiJsonResponseHandler<RIOResponse<[RIOUser]>>,
               let data = responser.responseObject {
                if data.success ?? false {
                    onComplete(data.data, nil)
                } else {
                    onComplete(nil, RIOApiError.apiError(data.code, data.message))
                }
            } else {
                onComplete(nil, RIOApiError.emptyResponseData)
            }
        }, onFailure: { sender, response, error in
            onComplete(nil, error)
        }, jsonResponseModel: RIOResponse<[RIOUser]>.self)
        try request.start()
    }

}
