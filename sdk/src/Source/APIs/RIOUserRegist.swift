//
//  RIOUserRegist.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import APICodable

public struct RIOUser: Decodable {

    public var name: String?
    public var users: [RIOUser]?
    public var identifier: Int?
    public var createdAt: String?
    public var updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case name
        case createdAt = "created_at"
        case users
        case identifier = "id"
        case updatedAt = "updated_at"
    }

}

final class RIOUserRegist {

    private struct RequestParam: Encodable {
        var token = ""
        var name = ""
    }

    static func registUser(name: String, token: String, onComplete: @escaping (RIOUser?, Error?) -> Void) throws {
        guard let url = URL(string: RIOAppConfiguration.shared.apiRoot + RIOAppConfiguration.shared.apiRegister)
        else { return }
        var param = RequestParam()
        param.token = token
        param.name = name
        let request = NWApiRequest(post: url, parameter: param, onSuccess: { sender, response in
            if let responser = response as? NWApiJsonResponseHandler<RIOResponse<RIOUser>>,
               let data = responser.responseObject {
                if data.success ?? false {
                    onComplete(data.data, nil)
                } else {
                    onComplete(nil, RIOApiError.apiError(data.code, data.message))
                }
            } else {
                onComplete(nil, RIOApiError.emptyResponseData)
            }
        }, onFailure: { sender, response, error in
            onComplete(nil, error)
        }, jsonResponseModel: RIOResponse<RIOUser>.self)
        try request.start()
    }

}
