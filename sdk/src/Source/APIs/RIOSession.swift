//
//  RIOAuth.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import APICodable

struct RIOSessionData: Codable {

    struct DefaultVideoBitrates: Codable {
        var high: Int?
        var low: Int?
        var standard: Int?
    }

    struct AudioQuality: Codable {
        var stereo: Bool?
        var opusMaxAverageBitrate: Int?
    }

    struct ResolutionLevel: Codable {
        var resolution: Int?
        var audioQuality: AudioQuality?
    }

    struct Resolution: Codable {
        enum Level: String {
            case low
            case medium
            case high
        }
        var high: ResolutionLevel?
        var low: ResolutionLevel?
        var medium: ResolutionLevel?
    }

    struct App: Codable {
        var defaultVideoBitrates: DefaultVideoBitrates?
        var defaultVideoResolution: Int?
        var appId: String?
        var appName: String?
        var resolutions: Resolution?

        enum CodingKeys: String, CodingKey {
            case defaultVideoBitrates = "default_video_bitrates"
            case defaultVideoResolution = "default_video_resolution"
            case appId = "app_id"
            case appName = "app_name"
            case resolutions
        }
    }

    var token: String?
    var app: App?

}

final class RIOSession {

    private struct RequestParam: Encodable {
        var secretID = RIOAppConfiguration.shared.secretID
        var secretKey = RIOAppConfiguration.shared.secretKey

        private enum CodingKeys: String, CodingKey {
            case secretID = "secret_id"
            case secretKey = "secret_key"
        }
    }

    static func getSession(_ onComplete: @escaping (RIOSessionData?, Error?) -> Void) throws {
        guard let url = URL(string: RIOAppConfiguration.shared.apiRoot + RIOAppConfiguration.shared.apiSession)
        else { return }
        let param = RequestParam()
        let request = NWApiRequest(post: url, parameter: param, onSuccess: { sender, response in
            if let responser = response as? NWApiJsonResponseHandler<RIOResponse<RIOSessionData>>,
               let data = responser.responseObject {
                if data.success ?? false {
                    onComplete(data.data, nil)
                } else {
                    onComplete(nil, RIOApiError.apiError(data.code, data.message))
                }
            } else {
                onComplete(nil, RIOApiError.emptyResponseData)
            }
        }, onFailure: { sender, response, error in
            onComplete(nil, error)
        }, jsonResponseModel: RIOResponse<RIOSessionData>.self)
        try request.start()
    }

}
