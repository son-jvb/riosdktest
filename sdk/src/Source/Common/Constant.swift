//
//  Constant.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

/// SDK configuration
public final class RIOAppConfiguration {

    public static let shared = RIOAppConfiguration()

    public var apiRoot = ""
    public var apiSession = "/sdk/session"
    public var apiRegister = "/sdk/user/register"
    public var apiUserList = "/sdk/user/list"
    public var apiFileList = "/sdk/file/list"
    public var apiFileDownload = "/sdk/file/download"
    public var apiFileUpload = "/sdk/system/file/upload"
    public var apiLogin = "/sdk/system/user/login"
    public var apiCallStart = "/sdk/system/call/start"
    public var apiCallAddLog = "/sdk/system/call/add-log"
    public var socketUrl = ""

    public var socketPath = "/app"
    public var secretID = ""
    public var secretKey = ""

    public var socketAutoReconnect = true
    public var socketMaxRetry = 1
    public var socketTimeout: TimeInterval = -1 // 20 // seconds
    public var socketReconnectionDelay = 1 // seconds
    public var socketCredentials = true
    public var socketCallTimeout: TimeInterval = 30 // seconds
    public var socketSessionConfig: URLSessionConfiguration?

    public enum ConfigError: Error {
        /// Root URL for API is empty or invalid
        case apiRoot
        /// Fail to make API URL
        case apiUrl
        /// Socket URL for API is empty or invalid
        case socketUrl
        /// Secret ID or Secret Key is empty or invalid
        case secret
    }

    func validate() throws {
        if URL(string: apiRoot) == nil {
            throw ConfigError.apiRoot
        }
        if URL(string: apiRoot + apiSession) == nil ||
            URL(string: apiRoot + apiRegister) == nil ||
            URL(string: apiRoot + apiUserList) == nil ||
            URL(string: apiRoot + apiFileList) == nil ||
            URL(string: apiRoot + apiFileDownload) == nil ||
            URL(string: apiRoot + apiFileUpload) == nil ||
            URL(string: apiRoot + apiLogin) == nil ||
            URL(string: apiRoot + apiCallStart) == nil ||
            URL(string: apiRoot + apiCallAddLog) == nil {
            throw ConfigError.apiUrl
        }
        if URL(string: socketUrl) == nil {
            throw ConfigError.socketUrl
        }
        if secretID.count == 0 || secretKey.count == 0 {
            throw ConfigError.secret
        }
    }
}
