//
//  RIOCallManager.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import UIKit

/// Call object
public struct RIOCallItem {

    public enum CallResponse: String {
        case accepted
        case timeout
        case rejected
    }

    /// Identifier to handle
    public let identifier: String
    /// Caller ID
    public let callerId: Int
    /// Receiver ID
    public let receiverId: Int
    /// Session ID of other side user
    var sessionID: String?
    /// Room to chat
    var room: String?

    var isConnected: Bool {
        return room != nil
    }
}

/// Call manager
final class RIOCallManager {

    static let shared = RIOCallManager()

    private init() {}

    enum CallType: String {
        case incomming = "I"
        case outgoing = "O"
    }

    private var callList = [RIOCallItem]()

    func newCall(type: CallType, fromUserId: Int, toUserId: Int) -> RIOCallItem {
        let prefix = type.rawValue
        var idx = 1
        while true {
            if callList.contains(where: {  $0.identifier == prefix + "\(idx)" }) {
                idx += 1
            } else {
                break
            }
        }
        let item = RIOCallItem(identifier: prefix + "\(idx)",
                               callerId: fromUserId, receiverId: toUserId)
        callList.append(item)
        return item
    }

    func callConnected(itemId: String, room: String) {
        if let index = callList.firstIndex(where: { $0.identifier == itemId }) {
            callList[index].room = room
        }
    }

    func endCall(itemId: String) {
        callList.removeAll { $0.identifier == itemId }
    }

    func contains(itemId: String) -> Bool {
        return callList.contains { $0.identifier == itemId }
    }

    func findCall(fromUserId: Int, toUserId: Int) -> RIOCallItem? {
        return callList.first { $0.callerId == fromUserId && $0.receiverId == toUserId }
    }

    func getCall(callId: String) -> RIOCallItem? {
        return callList.first { $0.identifier == callId }
    }

    func reset() {
        callList.removeAll()
    }

}
