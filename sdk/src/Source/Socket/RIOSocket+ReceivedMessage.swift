//
//  RIOSocket+ReceivedMessage.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import CommonLog

extension RIOSocket {

    /// Handle incoming socket message
    func handleMessage(_ object: RIOSocketData) {
        CMLog("SOCKET receives event", object)
        switch object.eventType {
        case .message:
            guard let mesg = object.decodeJson(type: RIOSocketMessage.self) else { return }
            switch mesg.action {
            case .register:
                handleRegistUserResponse(mesg)
            case .callStop:
                handleCallStop(mesg)
            case .callIncoming:
                handleIncomingCall(mesg)
            case .callResposne:
                handleCallResponse(mesg)
            case .callStart:
                handleCallStart(mesg)
            default:
                break
            }
        default:
            break
        }
    }

    /// Response after regist user
    private func handleRegistUserResponse(_ mesg: RIOSocketMessage) {
        guard mesg.sessionId == sessionId else { return }
        if mesg.response == RIOCallItem.CallResponse.accepted.rawValue {
            CMLog("SOCKET connects successfully")
            state = .connected
        } else {
            CMLog("SOCKET fails to connect")
            execOnMain {
                NotificationCenter.default.post(name: RIOSocketNotificationName.Error,
                                                object: RIOApp.shared,
                                                userInfo: [RIOSocketNotificationName.InfoKey.Error: RIOSocketError.userRejected])
            }
            closeSocket()
        }
    }

    /// Call-room ends
    private func handleCallStop(_ mesg: RIOSocketMessage) {
        guard let fromId = mesg.from, let toId = mesg.to,
              let call = RIOCallManager.shared.findCall(fromUserId: fromId, toUserId: toId) else { return }
        CMLog("SOCKET call stops:", "From: \(fromId)", "To: \(toId)")
        RIOCallManager.shared.endCall(itemId: call.identifier)
        execOnMain {
            NotificationCenter.default.post(name: RIOSocketNotificationName.CallStop,
                                            object: RIOApp.shared,
                                            userInfo: [RIOSocketNotificationName.InfoKey.Call: call])
        }
    }

    /// Incoming call
    private func handleIncomingCall(_ mesg: RIOSocketMessage) {
        guard let fromId = mesg.from, let toId = mesg.to, toId == userId else { return }
        let call = RIOCallManager.shared.newCall(type: .incomming, fromUserId: fromId, toUserId: toId)
        execOnMain {
            NotificationCenter.default.post(name: RIOSocketNotificationName.IncomingCall,
                                            object: RIOApp.shared,
                                            userInfo: [RIOSocketNotificationName.InfoKey.Call: call])
        }
    }

    /// Receiver accepted, rejected or timeout ...
    private func handleCallResponse(_ mesg: RIOSocketMessage) {
        guard let fromId = mesg.from, let toId = mesg.to,
              let call = RIOCallManager.shared.findCall(fromUserId: fromId, toUserId: toId) else { return }
        CMLog("SOCKET call response:", "From: \(fromId)", "To: \(toId)", "Response: \(mesg.response ?? "")", "Room: \(mesg.roomName ?? "")")
        if mesg.response == RIOCallItem.CallResponse.accepted.rawValue, let room = mesg.roomName {
            RIOCallManager.shared.callConnected(itemId: call.identifier, room: room)
            execOnMain {
                NotificationCenter.default.post(name: RIOSocketNotificationName.CallAccepted,
                                                object: RIOApp.shared,
                                                userInfo: [RIOSocketNotificationName.InfoKey.Call: call,
                                                           RIOSocketNotificationName.InfoKey.Room: room])
            }
        } else {
            RIOCallManager.shared.endCall(itemId: call.identifier)
            execOnMain {
                NotificationCenter.default.post(name: RIOSocketNotificationName.CallStop,
                                                object: RIOApp.shared,
                                                userInfo: [RIOSocketNotificationName.InfoKey.Call: call,
                                                           RIOSocketNotificationName.InfoKey.Response: mesg.response ?? "",
                                                           RIOSocketNotificationName.InfoKey.Message: mesg.message ?? ""])
            }
        }
    }

    /// Call start (after send accept call)
    private func handleCallStart(_ mesg: RIOSocketMessage) {
        guard let fromId = mesg.from, let toId = userId, let room = mesg.roomName,
              let call = RIOCallManager.shared.findCall(fromUserId: fromId, toUserId: toId) else { return }
        CMLog("SOCKET call starts:", "From: \(fromId)", "To: \(toId)", "Room: \(mesg.roomName ?? "")")
        RIOCallManager.shared.callConnected(itemId: call.identifier, room: room)
        execOnMain {
            NotificationCenter.default.post(name: RIOSocketNotificationName.CallStart,
                                            object: RIOApp.shared,
                                            userInfo: [RIOSocketNotificationName.InfoKey.Call: call,
                                                       RIOSocketNotificationName.InfoKey.Room: room])
        }
    }

}
