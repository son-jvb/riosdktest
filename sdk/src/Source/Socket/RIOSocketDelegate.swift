//
//  RIOSocketDelegate.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

/// State of socket
public enum RIOSocketState {

    /// Socket does not connect
    case disconnected
    /// Socket is connecting
    case connecting
    /// Socket connected
    case connected

}

extension RIOSocketState: CustomStringConvertible {

    public var description: String {
        switch self {
        case .disconnected:
            return "RIOSocket disconnected."
        case .connected:
            return "RIOSocket connected."
        case .connecting:
            return "RIOSocket is connecting."
        }
    }
}

/// Notification Event from socket
public struct RIOSocketNotificationName {

    /// Socket connection changing event
    ///
    /// UserInfo: `OldState` & `NewState`
    static public let ChangeState = Notification.Name("RIOSocket.State")
    /// Socket error event
    ///
    /// UserInfo: `Error`
    static public let Error = Notification.Name("RIOSocket.Error")
    /// Socket get phone call event (icoming phone call)
    ///
    /// UserInfo: `Call`
    static public let IncomingCall = Notification.Name("RIOSocket.IncomingCall")
    /// Socket get call-stop event (end calling room, recevier reject or timeout)
    ///
    /// UserInfo: `Call` & `Response` & `Message`
    static public let CallStop = Notification.Name("RIOSocket.CallStop")
    /// Receiver accepts call
    ///
    /// UserInfo: `Call` & `Room`
    static public let CallAccepted = Notification.Name("RIOSocket.CallAccepted")
    /// Received room name to start the call
    ///
    /// UserInfo: `Call` & `Room`
    static public let CallStart = Notification.Name("RIOSocket.CallStart")


    /// Key for UserInfo of Notification
    public struct InfoKey {
        /// Previous socket connection state.
        ///
        /// Value type: `RIOSocketState`.
        static public let OldState = "OldState"
        /// Current socket connection state
        ///
        /// Value type: `RIOSocketState`.
        static public let NewState = "NewState"
        /// Socket error.
        ///
        /// Value type: `Error`.
        static public let Error = "Error"
        /// Call info.
        ///
        /// Value type: `RIOCallItem`.
        static public let Call = "Call"
        /// Call response status.
        ///
        /// Value type: `String`.
        static public let Response = "Response"
        /// Message.
        ///
        /// Value type: `String`
        /// `.
        static public let Message = "Message"
        /// Room name.
        ///
        /// Value type: `String`
        /// `.
        static public let Room = "Room"
    }

}
