//
//  RIOSocket+Tasks.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import CommonLog

extension RIOSocket {

    /// Seng pong (answer Ping message from server)
    func sendPong() {
        let object = RIOSocketData.pong
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Send regist user message
    func sendRegistUser() {
        let mesg = RIOSocketMessage.makeRegistUser(appId: appId ?? "", name: "\(userId ?? 0)")
        let object = RIOSocketData.makeMessage(data: mesg, nameSpace: socketNameSpace ?? "")
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Send App Auth message
    func sendSocketAuth() {
        var object = RIOSocketData()
        object.statusType = .auth
        object.nameSpace = socketNameSpace
        object.importJson(object: ["X_APP_ID": appId ?? ""])
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Start a call to given user id
    func sendCall(toUser: Int) {
        var object = RIOSocketData.makeMessageEvent(nameSpace: socketNameSpace ?? "")
        var mesg = RIOSocketMessage()
        mesg.appId = appId
        mesg.from = userId
        mesg.to = toUser
        mesg.action = .call
        mesg.token = token
        mesg.timeout = RIOAppConfiguration.shared.socketCallTimeout
        mesg.sdpOffer = RIOSdpOffer()
        object.importJson(object: mesg)
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Send call-end (both incoming/outgoing call)
    func sendStopCall(fromUserId: Int, toUserId: Int) {
        var object = RIOSocketData.makeMessageEvent(nameSpace: socketNameSpace ?? "")
        var mesg = RIOSocketMessage()
        mesg.appId = appId
        mesg.action = .stop
        mesg.from = fromUserId
        mesg.to = toUserId
        mesg.userIdInt = userId
        object.importJson(object: mesg)
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Receiver accepts incoming call
    func sendAcceptIncomingCall(fromUserId: Int) {
        var object = RIOSocketData.makeMessageEvent(nameSpace: socketNameSpace ?? "")
        var mesg = RIOSocketMessage()
        mesg.appId = appId
        mesg.action = .incomingCallResponse
        mesg.from = fromUserId
        mesg.to = userId
        mesg.userIdInt = userId
        mesg.sdpOffer = RIOSdpOffer()
        mesg.callResponse = RIOCallItem.CallResponse.accepted.rawValue
        object.importJson(object: mesg)
        guard let data = object.packetText else { return }
        sendText(data)
    }

    /// Receiver rejects incoming call
    func sendRejectIncommingCall(fromUserId: Int) {
        var object = RIOSocketData.makeMessageEvent(nameSpace: socketNameSpace ?? "")
        var mesg = RIOSocketMessage()
        mesg.action = .incomingCallResponse
        mesg.appId = appId
        mesg.from = fromUserId
        mesg.to = userId
        mesg.userIdInt = userId
        mesg.callResponse = RIOCallItem.CallResponse.rejected.rawValue
        mesg.sdpOffer = RIOSdpOffer()
        object.importJson(object: mesg)
        guard let data = object.packetText else { return }
        sendText(data)
    }

    func sendRoomStart(toUserId: Int) {
        var object = RIOSocketData.makeMessageEvent(nameSpace: socketNameSpace ?? "")
        var mesg = RIOSocketMessage()
        mesg.action = .roomStart
        mesg.appId = appId
        mesg.from = userId
        mesg.to = toUserId
        mesg.reconnected = 0
        mesg.userIdInt = userId
        object.importJson(object: mesg)
        guard let data = object.packetText else { return }
        sendText(data)
    }

}
