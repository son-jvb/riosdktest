//
//  RIOSocketModel.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import CommonLog

// Socket sample
// <Status><namespace = ","><JSON: [] or {}>

struct RIOSocketData {

    enum StatusType: String {
        case greeting = "0"
        case ping = "2"
        case pong = "3"
        case auth = "40"
        case event = "42"
    }

    enum EventName: String {
        case message
    }

    var status: String?
    var nameSpace: String?
    var json: String?
    var eventName: String?

    var statusType: StatusType? {
        get {
            return StatusType(rawValue: status ?? "")
        }
        set {
            status = newValue?.rawValue
        }
    }

    var eventType: EventName? {
        get {
            return EventName(rawValue: eventName ?? "")
        }
        set {
            eventName = newValue?.rawValue
        }
    }

    static func makeMessageEvent(nameSpace: String) -> RIOSocketData {
        var result = RIOSocketData()
        result.statusType = .event
        result.nameSpace = nameSpace
        result.eventType = .message
        return result
    }

    init() {}

    init?(data: Data) {
        guard let dataStr = String(data: data, encoding: .utf8) else { return nil }
        var prefix: String?
        if let range1 = dataStr.range(of: "["), let range2 = dataStr.range(of: "{") {
            let bound = min(range1.lowerBound, range2.lowerBound)
            prefix = String(dataStr[..<bound])
            json = String(dataStr[bound...])
        } else if let range = dataStr.range(of: "[") {
            prefix = String(dataStr[..<range.lowerBound])
            json = String(dataStr[range.lowerBound...])
        } else if let range = dataStr.range(of: "{") {
            prefix = String(dataStr[..<range.lowerBound])
            json = String(dataStr[range.lowerBound...])
        } else {
            prefix = dataStr
        }
        if let str = prefix, let range = str.range(of: "/") {
            status = String(str[..<range.lowerBound])
            let res = String(str[range.lowerBound...])
            if res.hasSuffix(",") {
                nameSpace = String(res[..<res.index(before: res.endIndex)])
            } else {
                nameSpace = res
            }
        } else {
            status = prefix
        }
        if statusType == .event {
            if let data = json?.data(using: .utf8),
               let array = try? JSONSerialization.jsonObject(with: data, options: .init(rawValue: 0)) as? NSArray,
               array.count == 2 {
                eventName = array.firstObject as? String
                if let str = array.lastObject as? String {
                    json = str
                } else if let obj = array.lastObject,
                          let data = try? JSONSerialization.data(withJSONObject: obj, options: .init(rawValue: 0)),
                          let str = String(data: data, encoding: .utf8) {
                    json = str
                }
            }
        }
    }

    var packetText: String? {
        guard var result = status else { return nil }
        if let nSpc = nameSpace, nSpc.count > 0 {
            result += nSpc + ","
        }
        if let event = eventName {
            var array = [event]
            if let data = json {
                array.append(data)
            }
            let encoder = JSONEncoder()
            if let data = try? encoder.encode(array), let str = String(data: data, encoding: .utf8) {
                result += str
            }
        } else if let data = json {
            result += data
        }
        return result
    }

    func decodeJson<DataType: Decodable>(type: DataType.Type) -> DataType? {
        guard let data = json?.data(using: .utf8) else { return nil }
        let decoder = JSONDecoder()
        do {
            return try decoder.decode(type, from: data)
        } catch let err {
            CMLog("Fail to decode JSON:", type, json, err)
            return nil
        }
    }

    mutating func importJson<DataType: Encodable>(object: DataType) {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(object) {
            json = String(data: data, encoding: .utf8)
        }
    }

}

extension RIOSocketData {

    static var pong: RIOSocketData {
        var result = RIOSocketData()
        result.status = StatusType.pong.rawValue
        return result
    }

    static func makeMessage<DataType: Encodable>(data: DataType, nameSpace: String) -> RIOSocketData {
        var result = RIOSocketData()
        result.statusType = .event
        result.eventType = .message
        result.nameSpace = nameSpace
        result.importJson(object: data)
        return result
    }

}
