//
//  RIOSocket.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation
import CommonLog
import Network

/// Socket error
public enum RIOSocketError: Error {

    /// Fail to regist user
    case userRejected

}

/// Socket handler
final class RIOSocket: NSObject {

    static let shared = RIOSocket()

    private var socket: URLSessionWebSocketTask?
    private var session: URLSession!

    private override init() {
        super.init()
        session = URLSession(configuration: RIOAppConfiguration.shared.socketSessionConfig ?? .default,
                             delegate: self, delegateQueue: nil)
    }

    var token: String?
    private var socketUrl: URL?
    var socketNameSpace: String?
    var appId: String?
    var userId: Int?
    var sessionId: String?

    var state: RIOSocketState = .disconnected {
        didSet {
            if oldValue != state {
                let oldState = oldValue
                let newState = state
                execOnMain {
                    NotificationCenter.default.post(name: RIOSocketNotificationName.ChangeState,
                                                    object: RIOApp.shared,
                                                    userInfo: [RIOSocketNotificationName.InfoKey.OldState: oldState,
                                                               RIOSocketNotificationName.InfoKey.NewState: newState])
                }
            }
        }
    }

    deinit {
        closeSocket()
    }

    func execOnMain(_ action: @escaping () -> Void) {
        if Thread.isMainThread {
            action()
        } else {
            DispatchQueue.main.async(execute: action)
        }
    }

    /// Disconnect socket
    func closeSocket() {
        socket?.cancel()
        socket = nil
        state = .disconnected
        RIOApp.shared.socketDidDisconnect()
    }

    /// Start connect socket
    func openSocket(appId: String, userId: Int, token: String) throws {
        closeSocket()
        try RIOAppConfiguration.shared.validate()
        let cfg = RIOAppConfiguration.shared
        self.userId = userId
        self.appId = appId
        self.token = token
        socketNameSpace = cfg.socketPath + "/" + appId
        socketUrl = URL(string: cfg.socketUrl + (socketNameSpace ?? "") + "?transport=websocket&EIO=4")
        guard let url = socketUrl else { return }
        CMLog(url)
        let sock = session.webSocketTask(with: url)
        sock.resume()
        socket = sock
        state = .connecting
    }

    private func readSocket() {
        socket?.receive {[weak self] result in
            guard let mSelf = self else { return }
            switch result {
            case .success(let mesg):
                switch mesg {
                case .string(let text):
                    CMLog("SOCKET read text", text)
                    if let data = text.data(using: .utf8) {
                        mSelf.processMessage(data: data)
                    }
                case .data(let data):
                    CMLog("SOCKET read data", data.count, String(data: data, encoding: .utf8))
                    mSelf.processMessage(data: data)
                @unknown default:
                    CMLog("SOCKET read unknown", mesg)
                }
            case .failure(let err):
                CMLog("SOCKET read error", err)
            }
            mSelf.readSocket()
        }
    }

    func sendText(_ text: String) {
        if text == "3" {
            CMLog("SOCKET sends pong message")
        } else {
            CMLog("SOCKET sends", text)
        }
        socket?.send(.string(text), completionHandler: {(err) in
            CMLog("SOCKET did send", text, err)
        })
    }

    private func processMessage(data: Data) {
        guard let object = RIOSocketData(data: data)
        else { return }
        switch object.statusType {
        case .greeting:
            CMLog("SOCKET receives greeting message.")
            sendSocketAuth()
        case .ping:
            CMLog("SOCKET receives ping message.")
            sendPong()
        case .auth:
            if let session = object.decodeJson(type: RIOSocketSession.self) {
                CMLog("SOCKET receives Session message.", session.sid)
                sessionId = session.sid
                sendRegistUser()
            }
        case .event:
            handleMessage(object)
        case .none:
            break
        default:
            break
        }
    }
 
}

extension RIOSocket: URLSessionDelegate, URLSessionWebSocketDelegate {

    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocolName: String?) {
        CMLog("SOCKET connected!")
        readSocket()
    }

    func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        if session == self.session && webSocketTask == self.socket {
            closeSocket()
        }
    }

}
