//
//  RIOSocketModel.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

    //list event type
//WS_EVENT_CONNECTION: 'connection',
//WS_EVENT_REGISTER: 'register',
//WS_EVENT_CALL_INCOMING: 'incoming',
//WS_EVENT_RESPONSE: 'response',
//WS_EVENT_CALL_START: 'start',
//WS_EVENT_CALL_STOP: 'stop',
//WS_EVENT_CHANGE_QUALITY: 'change.quality',
//WS_EVENT_CALL_RECONNECT: 'call.reconnected',
//WS_EVENT_VOICE_CHANGE: 'voice.change',
//WS_EVENT_ERROR: 'error',
//
//    //action send from websocket server
//WS_REGISTER_REJECTED: 'register.accepted',
//WS_CALL_ACCEPTED: 'accepted',
//WS_CALL_ACCEPTED_OTHER: 'other.accepted',
//WS_CALL_REJECTED: 'rejected',
//WS_CALL_REJECTED_OTHER: 'other.rejected',
//WS_CALL_TIMEOUT: 'timeout',
//WS_CALL_BUSY: 'busy',
//WS_CALL_CANCEL: 'cancel',
//WS_ACT_REGISTER: 'register',
//WS_ACT_LIST: 'list',
//WS_ACT_CALL_INCOMING: 'call.incoming',
//WS_ACT_CALL_START: 'call.start',
//WS_ACT_CALL_STOP: 'call.stop',
//WS_ACT_CALL_FORCE_STOP: 'call.force.stop',
//WS_ACT_CALL_RESPONSE: 'call.response',
//WS_ACT_ICE_CANDIDATE: 'iceCandidate',
//WS_ACT_MSG_INCOMING: 'msg.incoming',
//WS_ACT_CHANGE_QUALITY: 'change.quality',
//WS_ACT_VOICE_CHANGE: 'voice.change',
//WS_ACT_ERROR: 'error',

    /**
     * list action send to web socket
     */
//WS_SEND_LIST: 'list',
//WS_SEND_STOP: 'stop',
//WS_SEND_ANSWER: 'answer',
//WS_SEND_NOT_ANSWER: 'not.answer',
//WS_SEND_CALL: 'call',
//WS_SEND_PING: 'ping',
//WS_SEND_ON_ICE_CANDIDATE: 'onIceCandidate',
//WS_SEND_ROOM_START: 'room.start',
//WS_SEND_INCOMING_RESPONSE: 'incomingCallResponse',
//WS_SEND_CHANGE_QUALITY: 'change.quality',
//WS_SEND_VOICE_CHANGE: 'voice.change',

struct RIOSdpOffer: Codable {

}

struct RIOSocketMessage: Codable {

    enum ActionType: String {
        case register
        case call
        case stop
        case callResposne = "call.response"
        case roomStart = "room.start"
        case changeQuality = "change.quality"
        case incomingCallResponse
        case callIncoming = "call.incoming"
        case callStart = "call.start"
        case callStop = "call.stop"
    }

    var appId: String?
    var secretId: String? = RIOAppConfiguration.shared.secretID
    var userIdInt: Int?
    var userIdStr: String?
    var from: Int?
    var to: Int?
    var act: String?
    var token: String?
    var name: String?
    var reconnection: Bool?
    var response: String?
    var sessionId: String?
    var timeout: TimeInterval?
    var callResponse: String?
    var message: String?
    var roomName: String?
    var callId: Int?
    var type: String?
    var reconnected: Int?
    var calleeId: Int?
    var sdpOffer: RIOSdpOffer?
    var offer: RIOSdpOffer?

    private enum CodingKeys: String, CodingKey {
        case appId
        case secretId
        case userId
        case from
        case to
        case act
        case token
        case name
        case reconnection
        case response
        case sessionId
        case timeout
        case callResponse
        case message
        case roomName
        case callId
        case type
        case reconnected
        case calleeId
        case sdpOffer
        case offer
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        appId = try? container.decode(String.self, forKey: .appId)
        secretId = try? container.decode(String.self, forKey: .secretId)
        from = try? container.decode(Int.self, forKey: .from)
        to = try? container.decode(Int.self, forKey: .to)
        act = try? container.decode(String.self, forKey: .act)
        token = try? container.decode(String.self, forKey: .token)
        name = try? container.decode(String.self, forKey: .name)
        reconnection = try? container.decode(Bool.self, forKey: .reconnection)
        response = try? container.decode(String.self, forKey: .response)
        sessionId = try? container.decode(String.self, forKey: .sessionId)
        timeout = try? container.decode(TimeInterval.self, forKey: .timeout)
        callResponse = try? container.decode(String.self, forKey: .callResponse)
        message = try? container.decode(String.self, forKey: .message)
        roomName = try? container.decode(String.self, forKey: .roomName)
        callId = try? container.decode(Int.self, forKey: .callId)
        type = try? container.decode(String.self, forKey: .type)
        reconnected = try? container.decode(Int.self, forKey: .reconnected)
        calleeId = try? container.decode(Int.self, forKey: .calleeId)
        sdpOffer = try? container.decode(RIOSdpOffer.self, forKey: .sdpOffer)
        offer = try? container.decode(RIOSdpOffer.self, forKey: .offer)

        userIdInt = try? container.decode(Int.self, forKey: .userId)
        userIdStr = try? container.decode(String.self, forKey: .userId)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if let value = appId {
            try? container.encode(value, forKey: .appId)
        }
        if let value = secretId {
            try? container.encode(value, forKey: .secretId)
        }
        if let value = from {
            try? container.encode(value, forKey: .from)
        }
        if let value = to {
            try? container.encode(value, forKey: .to)
        }
        if let value = act {
            try? container.encode(value, forKey: .act)
        }
        if let value = token {
            try? container.encode(value, forKey: .token)
        }
        if let value = name {
            try? container.encode(value, forKey: .name)
        }
        if let value = reconnection {
            try? container.encode(value, forKey: .reconnection)
        }
        if let value = response {
            try? container.encode(value, forKey: .response)
        }
        if let value = sessionId {
            try? container.encode(value, forKey: .sessionId)
        }
        if let value = timeout {
            try? container.encode(value, forKey: .timeout)
        }
        if let value = callResponse {
            try? container.encode(value, forKey: .callResponse)
        }
        if let value = message {
            try? container.encode(value, forKey: .message)
        }
        if let value = roomName {
            try? container.encode(value, forKey: .roomName)
        }
        if let value = callId {
            try? container.encode(value, forKey: .callId)
        }
        if let value = type {
            try? container.encode(value, forKey: .type)
        }
        if let value = reconnected {
            try? container.encode(value, forKey: .reconnected)
        }
        if let value = calleeId {
            try? container.encode(value, forKey: .calleeId)
        }
        if let value = sdpOffer {
            try? container.encode(value, forKey: .sdpOffer)
        }
        if let value = offer {
            try? container.encode(value, forKey: .offer)
        }

        if let value = userIdInt {
            try? container.encode(value, forKey: .userId)
        } else if let value = userIdStr {
            try? container.encode(value, forKey: .userId)
        }
    }

    var action: ActionType? {
        get {
            return ActionType(rawValue: act ?? "")
        }
        set {
            act = newValue?.rawValue
        }
    }

    var jsonData: Data? {
        let jsonEncoder = JSONEncoder()
        return try? jsonEncoder.encode(self)
    }

    init() {}

    static func parse(data: Data) -> RIOSocketMessage? {
        let decoder = JSONDecoder()
        return try? decoder.decode(RIOSocketMessage.self, from: data)
    }

    static func parse(text: String) -> RIOSocketMessage? {
        if let data = text.data(using: .utf8) {
            return parse(data: data)
        }
        return nil
    }

}

extension RIOSocketMessage {

    static func makeRegistUser(appId: String, name: String) -> RIOSocketMessage {
        var result = RIOSocketMessage()
        result.action = .register
        result.appId = appId
        result.name = name
        result.reconnection = false
        return result
    }

}
