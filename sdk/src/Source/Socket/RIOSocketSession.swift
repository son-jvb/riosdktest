//
//  RIOSocketSession.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

struct RIOSocketSession: Decodable {

    var sid: String?

}
