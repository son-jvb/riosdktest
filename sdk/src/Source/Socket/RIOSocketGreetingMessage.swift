//
//  RIOSocketGreetingMessage.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

struct RIOSocketGreetingMessage: Decodable {

    var sid: String?
    var upgrades: [String]?
    var pingInterval: TimeInterval?
    var pingTimeout: TimeInterval?

}
