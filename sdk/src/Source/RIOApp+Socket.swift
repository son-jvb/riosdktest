//
//  RIOApp+Socket.swift
//
//  Copyright © 2021 GMO-Z.com RunSystem JSC. All rights reserved.
//

import Foundation

/// Main manager object of SDK
extension RIOApp {

    func socketDidDisconnect() {
        RIOCallManager.shared.reset()
    }

    /** Open socket connection
     (Disconnect previous ones)

     - Parameters:
     - delegate: object receives socket events
     */
    public func openSocket() throws {
        guard let appId = userSession?.app?.appId, let userId = loggedInUserId, let token = userSession?.token else {
            throw AppError.noUserSession
        }
        try RIOSocket.shared.openSocket(appId: appId, userId: userId, token: token)
    }

    /// Disconnect socket
    public func closeSocket() {
        RIOSocket.shared.closeSocket()
    }

    /**
     Send call via socket

     Return: call id to end later
     */
    public func makeCall(toUserID: Int) throws -> RIOCallItem {
        guard let userId = loggedInUserId else {
            throw AppError.noUserSession
        }
        if socketState != .connected {
            throw AppError.socketDisconnected
        }
        RIOSocket.shared.sendCall(toUser: toUserID)
        return RIOCallManager.shared.newCall(type: .outgoing, fromUserId: userId, toUserId: toUserID)
    }

    /**
     End/Reject a call (incoming call or outgoing call)

     - Parameters:
        - callId: call identifier (given before)
     */
    public func endCall(callId: String) throws {
        guard let userId = loggedInUserId else {
            throw AppError.noUserSession
        }
        if socketState != .connected {
            throw AppError.socketDisconnected
        }
        guard let call = RIOCallManager.shared.getCall(callId: callId) else {
            throw RIOApp.AppError.callNotFound
        }
        if call.isConnected || call.callerId == userId { // call connected or call by current user (user is caller)
            RIOSocket.shared.sendStopCall(fromUserId: call.callerId, toUserId: call.receiverId)
        } else { // User is receiver
            RIOSocket.shared.sendRejectIncommingCall(fromUserId: call.callerId)
        }
        RIOCallManager.shared.endCall(itemId: callId)
    }

    /**
     Accept an incoming call

     - Parameters:
        - callId: call identifier (given before)
     */
    public func acceptCall(callId: String) throws {
        guard loggedInUserId != nil else {
            throw AppError.noUserSession
        }
        if socketState != .connected {
            throw AppError.socketDisconnected
        }
        guard let call = RIOCallManager.shared.getCall(callId: callId) else {
            throw RIOApp.AppError.callNotFound
        }
        RIOSocket.shared.sendAcceptIncomingCall(fromUserId: call.callerId)
    }

    /**
     Call this after the caller made room

     - Parameters:
        - callId: call identifier (given before)
     */
    public func roomStart(callId: String) throws {
        guard loggedInUserId != nil else {
            throw AppError.noUserSession
        }
        if socketState != .connected {
            throw AppError.socketDisconnected
        }
        guard let call = RIOCallManager.shared.getCall(callId: callId) else {
            throw RIOApp.AppError.callNotFound
        }
        RIOSocket.shared.sendRoomStart(toUserId: call.receiverId)
    }

}
