origin="${PODS_ROOT}/Headers/Public"
dest="${CONFIGURATION_BUILD_DIR}/${UNLOCALIZED_RESOURCES_FOLDER_PATH}/ExternalHeaders"

echo "$origin"
echo "$dest"

if [ -d "$dest" ]; then
  rm -r "$dest"
fi

cp -aL "$origin" "$dest"
