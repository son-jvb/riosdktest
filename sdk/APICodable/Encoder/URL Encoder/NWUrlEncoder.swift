//
//  NWUrlEncoder.swift
//
//  Created by DươngPQ on 04/01/2019.
//  Copyright © 2019 GMO-Z.com RunSystem. All rights reserved.
//

import Foundation

/// Encode encodable model into query string for URL (`application/x-www-form-urlencoded`)
open class NWUrlEncoder: NWKeyPathEncoderDelegate {

    /// How to encode Bool type
    public enum BoolConversion {
        /// Bool to 0, 1
        case boolToInt
        /// Bool to false, true
        case boolToString
    }

    public let builder: NWUrlQueryBuilder
    /// Encode nil value as empty text (ex: `&key1=&key2=...`)
    public var allowNull = false
    public var boolConversionType = BoolConversion.boolToString
    private let encoder = NWKeyPathEncoder()

    public init() {
        builder = NWUrlQueryBuilder()
        encoder.delegate = self
    }

    public init(_ queryBuilder: NWUrlQueryBuilder) {
        builder = queryBuilder
        encoder.delegate = self
    }

    public func encode(_ value: Encodable) throws -> String {
        if value is [Any] {
            throw NWError.notSupport
        }
        builder.removeAll()
        try value.encode(to: encoder)
        let result = try builder.generate()
        return result
    }

    /// MARK: - Key-Path encoder delegate

    open func encode(value: String, keyPath: NWKeyPath) throws {
        let param = NWUrlQueryBuilder.QueryParameter(keyPath: keyPath, value: value)
        builder.addParamater(param)
    }

    public func encode(value: Bool, keyPath: NWKeyPath) throws {
        var conversion: String
        switch boolConversionType {
        case .boolToInt:
            conversion = value ? "1": "0"
        case .boolToString:
            conversion = value ? "true": "false"
        }
        try encode(value: conversion, keyPath: keyPath)
    }

    public func encode(value: Int, keyPath: NWKeyPath) throws {
        try encode(value: "\(value)", keyPath: keyPath)
    }

    public func encode(value: UInt, keyPath: NWKeyPath) throws {
        try encode(value: "\(value)", keyPath: keyPath)
    }

    public func encode(value: Double, keyPath: NWKeyPath) throws {
        try encode(value: "\(value)", keyPath: keyPath)
    }

    public func encodeNil(keyPath: NWKeyPath) throws -> Bool {
        if allowNull {
            try encode(value: "", keyPath: keyPath)
            return true
        }
        return false
    }

}
